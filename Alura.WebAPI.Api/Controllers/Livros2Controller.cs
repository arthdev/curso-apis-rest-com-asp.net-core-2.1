using System.Linq;
using Alura.ListaLeitura.Api.Modelos;
using Alura.ListaLeitura.Modelos;
using Alura.ListaLeitura.Persistencia;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Alura.ListaLeitura.Api.Controllers
{
    /*
        this attribute adds new behavoirs to the controller
        ex: automatically format responses when modelstate is invalid
        https://docs.microsoft.com/en-us/aspnet/core/web-api/?view=aspnetcore-5.0#apicontroller-attribute
    */
    [ApiController]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/livros")]
    [ApiExplorerSettings(GroupName = "v2")]
    [Authorize]
    public class Livros2Controller : ControllerBase
    {
        private readonly IRepository<Livro> _repo;

        public Livros2Controller(IRepository<Livro> repository)
        {
            _repo = repository;
        }

        [HttpGet]
        public IActionResult ListaDeLivros(
            [FromQuery] LivroFiltro filtro,
            [FromQuery] LivroOrdem ordem,
            [FromQuery] LivroPaginacao paginacao,
            [FromServices] Microsoft.AspNetCore.Hosting.IHostingEnvironment hostingEnvironment)
        {
            var livroPaginado = _repo.All
                .AplicaFiltro(filtro)
                .AplicaOrdem(ordem)
                .Select(l => l.ToApi())
                .ToLivroPaginado(paginacao);
            return Ok(livroPaginado);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(statusCode: 200, Type = typeof(LivroApi))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErrorResponse))]
        [ProducesResponseType(statusCode: 404, Type = typeof(ErrorResponse))]
        public IActionResult Recuperar([FromRoute] int id) {
            var model = _repo.Find(id);

            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        [HttpPost]
        [Consumes("multipart/form-data")]
        public IActionResult IncluirMultipartFormData([FromForm] LivroUpload model)
        {
            var livro = model.ToLivro();
            _repo.Incluir(livro);
            var uri = Url.Action("Recuperar", new { id = livro.Id });
            return Created(uri, livro);
        }

        [HttpPut]
        [Consumes("multipart/form-data")]
        public IActionResult AlterarMultipartFormdata([FromForm] LivroUpload model)
        {
            if (ModelState.IsValid)
            {
                var livro = model.ToLivro();
                if (model.Capa == null)
                {
                    livro.ImagemCapa = _repo.All
                        .Where(l => l.Id == livro.Id)
                        .Select(l => l.ImagemCapa)
                        .FirstOrDefault();
                }
                _repo.Alterar(livro);
                return Ok();
            }
            return UnprocessableEntity(ModelState);
        }

        [HttpDelete("{id}")]
        public IActionResult Remover([FromRoute] int id)
        {
            var model = _repo.Find(id);
            if (model == null)
            {
                return NotFound();
            }
            _repo.Excluir(model);
            return NoContent();
        }

        [HttpGet("{id}/capa")]
        public IActionResult ImagemCapa(int id)
        {
            byte[] img = _repo.All
                .Where(l => l.Id == id)
                .Select(l => l.ImagemCapa)
                .FirstOrDefault();
            if (img != null)
            {
                return File(img, "image/png");
            }
            return File(new byte[0],"image/png");
        }


        // Descomentar para também aceitar JSON
        // [HttpPut]
        // [Consumes("application/json")]
        // public IActionResult Alterar([FromBody] LivroUpload model)
        // {
        //     if (ModelState.IsValid)
        //     {
        //         var livro = model.ToLivro();
        //         if (model.Capa == null)
        //         {
        //             livro.ImagemCapa = _repo.All
        //                 .Where(l => l.Id == livro.Id)
        //                 .Select(l => l.ImagemCapa)
        //                 .FirstOrDefault();
        //         }
        //         _repo.Alterar(livro);
        //         return Ok();
        //     }
        //     return UnprocessableEntity(ModelState);
        // }

        // [HttpPost]
        // [Consumes("application/json")]
        // public IActionResult Incluir([FromBody] LivroUpload model)
        // {
        //     var livro = model.ToLivro();
        //     _repo.Incluir(livro);
        //     var uri = Url.Action("Recuperar", new { id = livro.Id });
        //     return Created(uri, livro);
        // }
    }
}