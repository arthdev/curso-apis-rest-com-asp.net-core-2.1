using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Alura.ListaLeitura.Modelos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;

namespace Alura.ListaLeitura.Api.Formatters
{
    public class LivroCsvFormatter : TextOutputFormatter
    {
        public LivroCsvFormatter()
        {
            IList<MediaTypeHeaderValue> suppertedMediaTypes = MediaTypeHeaderValue.ParseList(
                new List<string>()
                {
                    "text/csv",
                    "application/csv"
                }
            );
            this.SupportedMediaTypes.Add(suppertedMediaTypes[0]);
            this.SupportedMediaTypes.Add(suppertedMediaTypes[1]);

            SupportedEncodings.Add(Encoding.UTF8);
        }

        protected override bool CanWriteType(System.Type type)
        {
            return type == typeof(LivroApi);
        }

        public override Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            var livroEmCsv = "";

            if (context.Object is LivroApi)
            {
                var livro = context.Object as LivroApi;
                livroEmCsv = $"{livro.Titulo};{livro.Subtitulo};{livro.Autor};{livro.Lista}";
            }

            using (var escritor = context.WriterFactory(context.HttpContext.Response.Body, selectedEncoding))
            {
                return escritor.WriteAsync(livroEmCsv);
            }
        }
    }
}