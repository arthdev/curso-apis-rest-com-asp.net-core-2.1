using System.Collections.Generic;
using System.Linq;
using Alura.ListaLeitura.Modelos;

namespace Alura.ListaLeitura.Api.Modelos
{
    public static class LivroPaginoExtensions
    {
        public static LivroPaginado ToLivroPaginado(this IQueryable<LivroApi> query, LivroPaginacao paginacao)
        {
            int totalItens = query.Count();
            int totalPaginas = (int) System.Math.Ceiling(totalItens/(double)paginacao.Tamanho);
            return new LivroPaginado()
            {
                Total = totalItens,
                TotalPaginas = totalPaginas,
                NumeroPagina = paginacao.Pagina,
                TamanhoPagina = paginacao.Tamanho,
                Resultado = query.Skip(paginacao.Tamanho * (paginacao.Pagina - 1)).Take(paginacao.Tamanho).ToList(),
                Anterior = (paginacao.Pagina > 1) ?
                    $"livros?pagina={paginacao.Pagina - 1}&tamanho={paginacao.Tamanho}" :
                    "",
                Proxima = (paginacao.Pagina > totalPaginas) ?
                    $"livros?pagina={paginacao.Pagina + 1}&tamanho={paginacao.Tamanho}" :
                    ""
            };
        }
    }
    public class LivroPaginado
    {
        public int Total { get; set; }
        public int TotalPaginas { get; set; }
        public int TamanhoPagina { get; set; }
        public int NumeroPagina { get; set; }
        public IList<LivroApi> Resultado { get;set; }
        public string Anterior { get; set; }
        public string Proxima { get; set; }
    }
    public class LivroPaginacao
    {
        public int Pagina { get; set; } = 1;
        public int Tamanho { get; set; } = 25;

    }
}