﻿using Alura.ListaLeitura.Modelos;
using Alura.ListaLeitura.WebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Alura.ListaLeitura.HttpClients;
using System.Threading.Tasks;
using System.Linq;

namespace Alura.ListaLeitura.WebApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly LivroApiClient _apiClient;

        public HomeController(LivroApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        private async Task<IEnumerable<LivroApi>> ListaDoTipoAsync(TipoListaLeitura tipo)
        {
            var lista = await _apiClient.GetListaLeituraAsync(tipo);
            return lista.Livros;
        }

        public async Task<IActionResult> Index()
        {
            var model = new HomeViewModel
            {
                ParaLer = await ListaDoTipoAsync(TipoListaLeitura.ParaLer),
                Lendo = await ListaDoTipoAsync(TipoListaLeitura.Lendo),
                Lidos = await ListaDoTipoAsync(TipoListaLeitura.Lidos)
            };
            return View(model);
        }
    }
}